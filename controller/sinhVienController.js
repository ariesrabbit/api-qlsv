function layThongTinTuForm() {
  let ma = document.getElementById("txtMaSV").value;
  let ten = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let hinhAnh = document.getElementById("txtImg").value;

  return {
    ma: ma,
    ten: ten,
    email: email,
    hinhAnh: hinhAnh,
  };
}
var renderTable = function (list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var trContent = `
   <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td>
       <img src=${item.hinhAnh} style="width:70px" alt="" />
    </td>
    <td>
    <button
    onclick="xoaSinhVien('${item.ma}')"
    class="btn btn-danger">Xóa</button>
    <button
    onclick="suaSinhVien('${item.ma}')"
    class="btn btn-success">Sửa</button>
    </td>
  </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function showThongTinLenForm(data) {
  document.getElementById("txtTenSV").value = data.ten;
  document.getElementById("txtEmail").value = data.email;
  document.getElementById("txtImg").value = data.hinhAnh;
  document.getElementById("txtMaSV").value = data.ma;
}
