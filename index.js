var dssv = [];
// GET : để render
// PUT : để cập nhật
// DELETE : để xóa
var BASE_URL = "https://630092419a1035c7f8f46aac.mockapi.io";
// màn hình loading
var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};
// ...
var renderDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/dssv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
};
renderDssvService();
function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/dssv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      // chạy lại renderDssvService để render danh sách mới nhất
      renderDssvService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();

      console.log(err);
    });
}

function themSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/dssv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDssvService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function suaSinhVien(id) {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/dssv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function capNhatSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/dssv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
